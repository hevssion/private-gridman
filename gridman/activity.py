"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridsim.decorators import accepts, returns

import threading

class TerminalListener(object):
    def __init__(self):
        """
        __init__(self)

        listener for the terminal object
        """
        super(TerminalListener, self).__init__()

    @accepts(1, str)
    def new_terminal_data(self, data):
        """
        new_terminal_data(self,code,value)

        This function is called by the terminal listener when new data are available

        :param data: new data entered by the user on the console
        """
        raise NotImplementedError('Pure abstract method!')

    def terminal_end(self):
        """
        terminal_data(self)

        This function inform the listener that an exit key is pressed for the terminal thread
        """
        raise NotImplementedError('Pure abstract method!')


class Terminal(object):
    @accepts((1, dict))
    def __init__(self):
        """
        __init__(self)

        Listen on the console to grab input from the user and inform back the PQController
        """
        super(Terminal, self).__init__()

        # abort the listening
        self._abort = False

        # TerminalListener
        self._listener = []

    @accepts((1, TerminalListener))
    def add_listener(self, terminal_listener):
        """
        add_listener(self,pq_controller)

        :param terminal_listener: terminal listener to be notified on new data coming from the terminal
        """
        if isinstance(terminal_listener, TerminalListener):
            self._listener.append(terminal_listener)

    def abort(self):
        """
        abort(self)

        Abort the listening on the keyboard
        """
        self._abort = True

    def start(self):
        """
        start(self)

        Listen on the keyboard and inform back the pq_controller listener when new data are entered.
        EX: 5000a1p [SPACE] means 5000 active power (p) of the first district (1) on the first house (A)
        """
        while not self._abort:
            data = raw_input()
            if 'q' in data:
                for l in self._listener:
                    l.terminal_end()
            else:
                for l in self._listener:
                    l.new_terminal_data(data)

class ActivityListener:
    def __init__(self):

        self.activity_state = 0
        self.activity_lock = threading.Lock()

    def activity_new_state(self, state):
        raise NotImplementedError('Pure abstract method!')


class ActivityManager(TerminalListener):
    terminal = None
    def __init__(self):
        super(ActivityManager, self).__init__()

        self._listener = {}

    @accepts((1,str),(2,ActivityListener))
    def register_activity_listener(self, id, activity):
        self._listener[id] = activity

    def start_activity_manager(self):
        if ActivityManager.terminal is None:
            ActivityManager.terminal = Terminal()
            ActivityManager.terminal.add_listener(self)

            t = threading.Thread(target=ActivityManager.terminal.start)
            t.start()

    def new_terminal_data(self, data):
        args = data.split(' ')
        if len(args) > 0:
            if args[0] in self._listener.keys():
                self._listener[args[0]].activity_new_state(args[1:])

    def terminal_end(self):
        pass

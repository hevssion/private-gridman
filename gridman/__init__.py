import pkg_resources  # part of setuptools
__file__
__version__ = pkg_resources.require("Gridman")[0].version
